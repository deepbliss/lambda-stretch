<?php
/**
 * Template Name: Services Page
 *
 **/
get_header(); ?>


<section class="inner-banner" style='background: #fff url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.jpg<?php } ?>") no-repeat center center;'>
 <div class="inner-banner-text">
 <div class="slider-text">
 
 <h1> <?php the_field('inner_banner_headding') ?></h1>
 <p><?php the_field('inner_banner_text') ?> </p>
 
 </div>
 </div>
 </section>
<section class="service-sec">
	<div class="container">
		<div class="service-left">
            <ul>
				 <?php wp_nav_menu( array('menu' => 'Top Menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
			</ul>
        </div>
			
        <div class="service-right">
               <?php if ( have_posts() ) :
				while ( have_posts() ) : the_post();
				the_content();
				endwhile;
				endif; ?>
        </div>
        </div>
</section>





<?php get_footer(); ?>