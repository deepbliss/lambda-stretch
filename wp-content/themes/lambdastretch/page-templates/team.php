<?php
/**
 * Template Name: Team Page
 *
 **/
get_header(); ?>


<section class="inner-banner" style='background: #fff url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.jpg<?php } ?>") no-repeat center center;'>
 <div class="inner-banner-text">
 <div class="slider-text">
 
 <h1> <?php the_field('inner_banner_headding') ?></h1>
 <p><?php the_field('inner_banner_text') ?> </p>
 
 </div>
 </div>
 </section>
<section class="service-sec">
	<div class="container">
		<div class="service-left">
            <ul>
				 <?php wp_nav_menu( array('menu' => 'Top Menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
			</ul>
        </div>
			
        <div class="service-right">
               <?php if ( have_posts() ) :
				while ( have_posts() ) : the_post();
				the_content();
				endwhile;
				endif; ?>
                <div class="team-members">
                    <?php if( have_rows('team_partner_repetor') ): ?>

                    <ul class="team-list">  
                        <?php $i = 1; while( have_rows('team_partner_repetor') ): the_row();
                            // vars
                            $member_image = get_sub_field('member_image');
                            $member_name = get_sub_field('member_name');
                            $member_position = get_sub_field('member_position');
                            $member_link = get_sub_field('member_link');
                            $member_description = get_sub_field('member_description');               
                            ?>               
                        <li class="team">     
                           <div class="team-shown">
                                <a href="#popup<?php echo $i; ?>" class="fancybox" title="Sample title">    
                                    <div class="team-image">
                                        <img src="<?php echo $member_image['url']; ?>" alt="<?php echo $member_image['alt'] ?>" title="<?php echo $member_image['title'] ?>" />
                                         <div class="symbol wow">
                                             <svg viewBox="0 0 25 25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet">
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="Regular_LinkArrow" transform="translate(-20.000000, -18.000000)" fill="#221F20">
                                                        <polygon id="Regular_Arrow" points="39.107 26 37.692 27.414 41 30.9997 20 30.9997 20 32.9997 41 32.9997 37.692 36.726 39.107 38.14 45 31.9997"></polygon>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                   
                                    <div class="team-intro">              
                                        <h2><?php echo $member_name; ?></h2>
                                        <p><?php echo $member_position; ?></p>
                                    </div> 
                                    
                                    <div class="blurb">              
                                        <h2><?php echo $member_name; ?></h2>
                                        <p><?php echo $member_position; ?></p>
                                    </div>   
                               </a> 
                          </div> 
                           <div class="team-popup" id="popup<?php echo $i; ?>">
                             <div class="team-left"> 
                                 <img src="<?php echo $member_image['url']; ?>" alt="<?php echo $member_image['alt'] ?>" title="<?php echo $member_image['title'] ?>" />
                                 <div class="team-link"><a href="<?php echo $member_link; ?>" target="_blank">CONNECT ON LINKEDIN</a></div>                  
                             </div>
                             <div class="team-right"> 
                                <div class="team-right-header">                                                         
                                    <h2><?php echo $member_name; ?></h2>
                                    <h3><?php echo $member_position; ?></h3>
                                </div> 
                                <div class="team-right-content">                                 
                                     <p><?php echo $member_description; ?></p>
                                </div>       
                            </div>     
                           </div>    
                      </li>         
                           
                    <?php $i++; endwhile; ?>

                    </ul>

                <?php endif; ?>
        </div>    
        </div>
        </div>
</section>



<!--- Partner Section-->
<section class="partner">
	<div class="partner-left">
		 <h2><?php the_field('partner_heading') ?></h2>
 		 <p><?php the_field('partner_text') ?> </p> 
         
         <div class="partner-logo">    
            <img src="<?php the_field('exeter_logo'); ?>" alt="<?php the_field('exeter_logo'); ?>">
            <img src="<?php the_field('cranfield_logo'); ?>" alt="<?php the_field('cranfield_logo'); ?>" style="margin-right:0px;">                
        </div> 
	</div>
	
	<div class="partner-right">
		<img src="<?php the_field('partner_image'); ?>" alt="<?php the_field('partner_heading'); ?>">
	</div>            
</section>


<section class="partner-main">	
	<div class="container">	
        <img src="<?php the_field('partner_main'); ?>" alt="<?php the_field('partner_main'); ?>">                                                         
	</div>	
</section>

<?php get_footer(); ?>