<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section class="inner-banner" style='background: #fff url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.jpg<?php } ?>") no-repeat center center;'>
 <div class="inner-banner-text">
 <div class="slider-text">
 
 <h1> <?php the_field('inner_banner_headding') ?></h1>
 <p><?php the_field('inner_banner_text') ?> </p>
 
 </div>
 </div>
 </section>
<section class="inner-sec">
	<div class="container">
		<div class="inner-page">
                <h2><?php the_title(); ?></h2>
                <?php if ( have_posts() ) :
				while ( have_posts() ) : the_post();
				the_content();
				endwhile;
				endif; ?>
        </div>
        </div>
</section>

<?php get_footer();
