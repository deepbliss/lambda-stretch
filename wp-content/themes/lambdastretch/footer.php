<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>


<section class="home-contact">
	<div class="container">
		<div class="home-contact-inner">
			<h3>CONTACT US</h3>
			<div class="home-contact-left">
				<div class="home-contact-call"> <a href="tel:<?php the_field('contact_call', 'option'); ?>"><?php the_field('contact_call', 'option'); ?></a></div>
				<div class="home-contact-email"><a href="mailto:<?php the_field('contact_email', 'option'); ?>"> <?php the_field('contact_email', 'option'); ?></a></div>
				<div class="home-contact-address"><span> <?php the_field('contact_address', 'option'); ?></span></div>
			</div>
			<div class="home-contact-right">
				<?php echo do_shortcode('[contact-form-7 id="28" title="Contact form"]'); ?>
			</div>
		</div>
	</div>
</section>

	<footer>
		<div class="inner-footer">
			
				<div class="container">
					
					<div class="footer-lft">
						<div class="copyright">
							
								<p><?php the_field('copyright', 'option'); ?></a></p>
						</div>
						
						
					</div>	
					<div class="footer-rgt">
						<div  class="footer-menu">
						<ul>
							<?php wp_nav_menu( array('menu' => 'Footer Menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
						</ul>
						</div>
						<div class="footer-social-link">
							<ul>
								<li class="linkedin"><a href="<?php the_field('linkedin_url', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.png" alt="linkedin" title="linkedin"></a></li>
								<!--li class="twitter"><a href="<--?php the_field('twitter_url', 'option'); ?>" target="_blank"><img src="<--?php echo get_template_directory_uri(); ?>/images/twitter.png" alt="twitter" title="twitter"></a></li-->
                                <li class="digital"><a href="<?php the_field('digital_sale_url', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/digital.png" alt="digital sales" title="digital sales" rel="nofollow"></a></li>
                                                                
								
							</ul>
						</div>
					</div>		
							
							
						
				</div>
			
		</div>
	</footer>

</div>
   
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.min.all.js"></script>

<!--fancybox-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.min.js"></script>
<script>
    $(document).ready(function() {
       $(".fancybox").fancybox({
          openEffect: "none", 
          closeEffect: "none"
      });
    });
    
</script> 


<script type="text/javascript">
$(function() {
$('nav#menu').mmenu({
extensions : [ 'effect-slide-menu', 'pageshadow' ],
searchfield : false,
counters : false,
navbar : {
title : 'Menu'
},
navbars : [
{
position : 'top',
<!--content : [ 'searchfield' ]-->               
}, {
position : 'top',
content : [
'prev',
'title',
'close'
]
}
]
});
});

</script>

<script type="text/javascript">
$(window).scroll(function() {
if ($(this).scrollTop() > 1){  
    $('header').addClass("sticky");
  }
  else{
    $('header').removeClass("sticky");
  }
});
</script>


<script>
         document.addEventListener( 'wpcf7mailsent', function( event ) {
           window.location = "<?php echo site_url(); ?>/thank-you";
       }, false );
       </script>

<?php wp_footer(); ?>
</body>

</html>
