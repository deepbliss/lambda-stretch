<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section class="slider-sec">
	
		<?php do_shortcode('[slider]' ); ?>
 	
</section>
 
 <section class="home-service">
	<div class="container">
		<div class="home-inner-service">
			<ul>    
                  <?php if( have_rows('home_page_services') ):
                       while ( have_rows('home_page_services') ) : the_row(); ?>
                       <li>
						<a href="<?php the_sub_field('home_services_url'); ?>">
							
							<div class="hservice-img">
							   <figure>
								   <span>
									   <img src="<?php the_sub_field('home_service_image'); ?>" alt="<?php the_sub_field('home_service_title'); ?>">
								   </span>
							   </figure>
							  
						   </div>
						   <div class="hservice-head"><h2><?php the_sub_field('home_service_title'); ?></h2></div>
						    <div class="hservice-con"><p><?php the_sub_field('home_service_content'); ?></p></div>
						   </a>
                       </li>
                      <?php endwhile;
                  endif; ?>
            </ul>
		</div>
	</div>
</section>

<?php get_footer();
