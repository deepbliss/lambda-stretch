<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="https://gmpg.org/xfn/11">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content = "telephone=no">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1.0">
<meta name="description" content="Lambda Stretch">
<link rel="icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.ico" type="images/ico">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/mediaquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.fancybox.min.css">
<!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/css3-mediaqueries.js"  type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5shiv.js"  type="text/javascript"></script>
<![endif]-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
<script>
 new WOW().init();
</script>

	
<?php wp_head(); ?>
</head>
<?php global $more; 
	global $shortnametheme;
	$shortnametheme = "ls";
?>

<body <?php body_class(); ?>>
<div id="wrapper">
	<!--Header Start Here-->
	<header class="header <?php if (!is_front_page()){?>inner-header<?php }?>">	
        <div class="header_sec">
			<div class="container">
			
				<div class="mobile-nav">
                             <a class="menu-btn" href="#menu">
								<img src="<?php echo get_template_directory_uri(); ?>/images/menu-icon.png" alt="Menu Icon" title="Menu Icon" 
									class="menu-icon" >
								<img src="<?php echo get_template_directory_uri(); ?>/images/sticky-menu-icon.png" alt="Menu Icon" title="Menu Icon"
								class="sticky-menu-icon">
								<!--span>Menu</span-->
							</a>
							
                                <nav id="menu" >
									<ul>
										<?php wp_nav_menu( array('menu' => 'Top Menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
													
									</ul>
								</nav>	
				</div>	
				<div class="header-left">
				
					<div class="logo">
						<a href="<?php echo site_url(); ?>/">
						   
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" alt="Lambda Stretch" title="Lambda Stretch">
						  
						</a>
					</div>
					<div class="sticky-logo">
						<a href="<?php echo site_url(); ?>/">
						   
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sticky-logo.png" alt="Lambda Stretch" title="Lambda Stretch">
						  
						</a>
					</div>
				</div>
				<div class="header-right">
					<div class="desktop-menu">
						<nav>
						   <ul>                                                                      
							  <?php wp_nav_menu( array('menu' => 'Top Menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
							</ul>
						 </nav>
					</div>
				</div>
				
				
				
				
			</div>
			
		</div>
	</header>