<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'admin_lambdas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']{)W+=WY`$V2k~Fr@w3tF4Vy3*D~PPgBotB$sj,zAK1%Ytk=IE_f]-^1(X=y4SU.');
define('SECURE_AUTH_KEY',  '=o/SF{6`AQ0+o(@#&V_a!u]ym0Q?sSc<|hVK.{d;m7h -*Fb?3GD)?9b2BEb{!(1');
define('LOGGED_IN_KEY',    '8K%9p wIoZ]1;g:Lt.cF95b.1*i{/Y=4<ffQ+vVxi*->:mQom]!-S##!c[V48J^!');
define('NONCE_KEY',        '|/qmgMso]aVA8maTkVw`tHPOpv{ypcmc@VzqJi}*-4_P&VgH[3^_hUECs.P+.}r_');
define('AUTH_SALT',        ']C.=y&x ZL|g>98~_0n=XZ;!avQ<bM63?|]6.$(]/n k>BJe<2[34dXZhpRXw,Hr');
define('SECURE_AUTH_SALT', 'ADm3iztIKu<Y3Z`;;=yf8~|eY9N@rq6G[l#bk4({n}V5hIG)_c)h#wW&39u|n&qa');
define('LOGGED_IN_SALT',   '3nje /)egGLWg-)@oYpf`c*rqz.BY8lhJp)6I @B`EsXUzE3MKqL+(~BIhgs`U}[');
define('NONCE_SALT',       '{E6+8Uj {|1vzng^J`]rA;P|y#M26w/wKy9rB+6Y}l51=&qVh9~IbUB&yoGP!&-x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);